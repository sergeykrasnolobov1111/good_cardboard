$(document).ready(function () {
	$('.jsShowPopup').on('click', function(){
		$('#popup').show();
	});
	$('.popup-close').on('click', function(){
		$('.overlay').hide();
	});
	$('#header__btn').on('click', function(){
		yaCounter50775913.reachGoal('header__btn');
	});

	$('.jsPhone').mask('+7(999)999-99-99');

	$("form").on("submit", function(e){
		e.preventDefault();
		var form = $(this);

		$.ajax({
			url: 'mailer/mail.php',
			method: 'post',
			data: form.serialize(),
			success: function(){
				$(".overlay").hide();
				$("#thx").show();
				form.trigger("reset");
			}
		});
	});
	new WOW().init();

	$(".fabrication__slider_top").slick({
	  arrows: false,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  asNavFor: '.fabrication__slider_bottom',
	});
	$(".fabrication__slider_bottom").slick({
	  arrows: true,
	  slidesToShow: 4,
	  slidesToScroll: 1,
	  prevArrow: '<div class="slider-arrow fabrication__arrow fabrication__arrow_left"></div>',
	  nextArrow: '<div class="slider-arrow fabrication__arrow fabrication__arrow_right"></div>',
	  asNavFor: '.fabrication__slider_top',
	  responsive: [{
	      breakpoint: 992,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 768,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	  ]
	});


	$('.feedback__slider').slick({
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  prevArrow: '<div class="slider-arrow feedback__arrow feedback__arrow_left"></div>',
	  nextArrow: '<div class="slider-arrow feedback__arrow feedback__arrow_right"></div>',
	  responsive: [{
	      breakpoint: 992,
	      settings: {
	        slidesToShow: 1,
	      }
	    },
	    {breakpoint: 577,
	      settings: {
	        slidesToShow: 1, 
	      }
	    }
	  ]
	});
	});
