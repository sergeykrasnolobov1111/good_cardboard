module.exports = function (gulp, plugins, src) {
    return function () {
        plugins.browserSync.init({
            server: {
                baseDir: "public/"
            }
        });
    };
};